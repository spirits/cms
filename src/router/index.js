import Vue from 'vue'
import Router from 'vue-router'
import Resource from 'vue-resource'
import Layouts from '@/components/layouts'
import Index from '@/components/index/index'
import LoginUI from '@/components/login/login'
import Node from '@/components/node/index'
import Payment from '@/components/payment/index'
import Source from '@/components/source/index'
import System from '@/components/system/index'
import Role from '@/components/role/index'
import Manage from '@/components/manage/index'
import Assistant from '@/components/assistant/index'
import Sale from '@/components/sale/index'
import Course from '@/components/course/index'
import Order from '@/components/order/index'
import Detail from '@/components/detail/index'
import Finance from '@/components/finance/index'
import CourseService from '@/components/courseService/index'
import CourseServiceLog from '@/components/CourseServiceLog/index'
import Income from '@/components/income/index'
import TutorIncome from '@/components/tutorIncome/index'
import SaleGroup from '@/components/saleGroup/index'
import OrderLog from "@/components/OrderLog/index"
import Search from "@/components/search/index"
const storage = window.localStorage
Vue.use(Router)
Vue.use(Resource)



Vue.prototype.baseHost = "http://www.cms2.com"
Vue.prototype.login = function() {
  window.localStorage.clear();
  window.location.href="/"
}
Vue.prototype.writeStorageItem = function(token,data){
    window.localStorage.setItem(token,data)
}
Vue.prototype.readStorageItem = function(token){
    return window.localStorage.getItem(token)
}
Vue.prototype.formatData = function(result){
    if(result.data.hasOwnProperty('code')){
      if(result.data.code == 403){
          this.login();
      }else if (result.data.code == 402) {
        alert(result.data.message);
      }else if (result.data.code == 401){
        alert(result.data.message);
      }
      return false;
    }
    return result.data;
}
Vue.prototype.contains=function(arr,obj){
      var i = arr.length;
      while (i--) {
          if (arr[i] === obj) {
            return true;
          }
        }
       return false;
}
Vue.prototype.checkNum=function(val){
      var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    //var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/;
    if(regPos.test(val)){
        return true;
    }else{
        return false;
    }
}

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name:"main",
      component:Layouts,
      children:[
        {
          path:"",
          name:"index",
          component: Index
        },
        {
          path:"system",
          name:"system",
          component: System
        },
         {
          path:"role",
          name:"role",
          component: Role
        },
        {
          path:'node',
          name:'node',
          component:Node
        },
        {
          path:'payment',
          name:'payment',
          component:Payment
        },
        {
          path:'source',
          name:'source',
          component:Source
        },
        {
          path:'manage',
          name:"manage",
          component:Manage
        },
        {
          path:'assistant',
          name:'assistant',
          component:Assistant
        },
        {
          path:'sale',
          name:'sale',
          component:Sale
        },
        {
          path:"course",
          name:"course",
          component:Course
        },
        {
          path:"course-service",
          name:"course-service",
          component:CourseService
        },
        {
          path:"course-service-log/:id",
          name:"course-service-log",
          component:CourseServiceLog
        },
        {
          path:"order",
          name:"order",
          component:Order
        },
        {
          path:"log/:orderno",
          name:"log",
          component:OrderLog
        },
        {
          path:"order-detail/:orderno",
          name:"order-detail",
          component:Detail
        },
        {
          path:'finance',
          name:'finance',
          component:Finance
        },
        {
          path:'income',
          name:'income',
          component:Income
        },
        {
          path:'tutor-income',
          name:'tutor-income',
          component:TutorIncome
        },
        {
          path:"sale-group",
          name:"sale-group",
          component:SaleGroup
        },
        {
          path:'search',
          name:'search',
          component:Search
        }
      ],
      beforeEnter:(to,from,next)=>{
        storage.getItem('token') ? next() : next("/login")
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginUI
    }
  ]
})
